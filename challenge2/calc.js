const buttonResult = document.getElementById('result_button');
const inputNumbers = document.getElementById('text');

//запускается событие onclick
buttonResult.onclick = function() {
	 findMedian();
 };
 
//нахождение медианы
function findMedian() {
	let numbers = getNumbers();
	let median;
	let middle = Math.floor(numbers.length / 2);
	
	if (numbers.length % 2 === 1) {
		median = numbers[middle];
	} else {
		median = (numbers[middle-1] + numbers[middle]) / 2;
	}
	showResult(numbers, median);
 }

//получение строки из инпута
 function getNumbers() {
	 let numbers = convertToArray(inputNumbers.value);
	 numbers = bubbleSort(numbers);
	 
	 return numbers;
 }

//формируется массив из полученных чисел 
 function convertToArray(numbers) {
	 numbers = numbers.split(",");
	 let numbersArray = [];
	 
	 for (let i = 0; i < numbers.length; i++) {
		 numbersArray.push(+numbers[i])
	 }
	 return numbersArray;
 }

 //сортировка пузырьком
 function bubbleSort(numbers) {
	 let temp;
	 
	 for (let j = 0; j < numbers.length; j++) {
		 
		 for (let i = 0; i < numbers.length; i++) {
			 if (numbers[i] > numbers[i+1]) {
				 temp = numbers[i+1];
				 numbers[i+1] = numbers[i];
				 numbers[i] = temp;
			 }
		 }
	 }
	 return numbers;
 }
 
 //вывод результата на экран
 function showResult(numbers, median) {
	 
	 let answer = 'начальный массив: ' + inputNumbers.value +
				'<br> отсортированный массив: ' + numbers +
				'<br> медиана: ' + median;
				
	document.getElementById('result').innerHTML = answer;
 }
 
 
 
 
 
 
 
 
 
 
 
 