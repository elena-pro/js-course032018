const buttonResult = document.getElementById('result_button');
const numbersArrayDisplay = document.getElementById('result');



const number1 = document.getElementById('number1');
const number2 = document.getElementById('number2');
const number3 = document.getElementById('number3');

buttonResult.onclick = function() {

    const numberValue1 = +number1.value;
    const numberValue2 = +number2.value;
    const numberValue3 = +number3.value;

    let numbersArray = [numberValue1, numberValue2, numberValue3];
    numbersArray.sort((a, b) => { return a - b;});
    
	numbersArrayDisplay.innerHTML = numbersArray[1];
}