const buttonResultPlus = document.getElementById('result_plus');
const buttonResultMinus = document.getElementById('result_minus');
const count = document.getElementById('count');


function getActualCount() {
	let actualCount = +count.value;
	return actualCount;
} 

function findCountPlus(actualCount) {
	actualCount = ++actualCount;
	
	return actualCount;
} 

function findCountMinus(actualCount) {
	actualCount = --actualCount;
	
	if (actualCount < 0) {
		actualCount = 0;
	}
	
	return actualCount;
} 

buttonResultPlus.onclick = function() {
	let actualCount = getActualCount();
	actualCount = findCountPlus(actualCount);
	count.value = actualCount;
};

buttonResultMinus.onclick = function() {
	let actualCount = getActualCount();
	count.value = findCountMinus(actualCount);
};

